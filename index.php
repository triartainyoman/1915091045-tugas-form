<?php
if (isset($_GET['submit'])) {
  $nama = $_GET['nama'];
  $pelajaran = $_GET['pelajaran'];
  $nilai_uts = $_GET['uts'] * 0.35;
  $nilai_uas = $_GET['uas']  * 0.5;
  $nilai_tugas = $_GET['tugas'] * 0.15;

  $nilai_total = $nilai_uts + $nilai_uas + $nilai_tugas;

  $ulang = false;
  if ($nilai_total < 0 || $nilai_total > 100) {
    $ulang = true;
  } else {
    $grade;

    if ($nilai_total >= 90 && $nilai_total <= 100) {
      $grade = "A";
    } else if ($nilai_total > 70 && $nilai_total < 90) {
      $grade = "B";
    } else if ($nilai_total > 50 && $nilai_total <= 70) {
      $grade = "C";
    } else if ($nilai_total <= 50) {
      $grade = "D";
    } else {
      $grade = "Bapak Bangga Sekali";
    }
  }
}
?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <title>Sistem Penilaian Siswa</title>
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-dark bg-primary mb-5">
    <div class="container">
      <div class="container-fluid">
        <a class="navbar-brand d-flex justify-content-center" href="#">
          Sistem Penilaian Siswa
        </a>
      </div>
    </div>
  </nav>

  <?php if (isset($_GET['submit']) && $ulang == true) : ?>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8">
          <div text-white p-4>

          </div>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Anda Melakukan Kesalahan!</strong> Tolong masukan nilai 1 sampai 100 pada field nilai dibawah.
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-8 border rounded mt-3 p-4">
        <form action="" method="get">
          <div class="row mb-3">
            <label for="nama" class="col-sm-3 col-form-label">Nama Siswa</label>
            <div class="col-sm-9">
              <input type="text" name="nama" class="form-control" id="nama" autocomplete="off" required>
            </div>
          </div>
          <div class="row mb-3">
            <label for="pelajaran" class="col-sm-3 col-form-label">Mata Pelajaran</label>
            <div class="col-sm-9">
              <select class="form-select" name="pelajaran" aria-label="Default select example">
                <option value="Matematika">Matematika</option>
                <option value="Bahasa Inggris">Bahasa Inggris</option>
                <option value="Penjaskes">Penjaskes</option>
                <option value="Muatan Lokal">Muatan Lokal</option>
                <option value="Kewarganegaraan">Kewarganegaraan</option>
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <label for="uts" class="col-sm-3 col-form-label">Nilai UTS</label>
            <div class="col-sm-9">
              <input type="number" name="uts" class="form-control" id="uts" autocomplete="off" required>
            </div>
          </div>
          <div class="row mb-3">
            <label for="uas" class="col-sm-3 col-form-label">Nilai UAS</label>
            <div class="col-sm-9">
              <input type="number" name="uas" class="form-control" id="uas" autocomplete="off" required>
            </div>
          </div>
          <div class="row mb-3">
            <label for="tugas" class="col-sm-3 col-form-label">Nilai Tugas</label>
            <div class="col-sm-9">
              <input type="number" name="tugas" class="form-control" id="tugas" autocomplete="off" required>
            </div>
          </div>
          <div class="col-12 d-grid gap-2 d-md-flex justify-content-md-end">
            <button type="submit" name="submit" value="submit" class="btn btn-primary me-md-1">Hitung</button>
            <button type="reset" class="btn btn-danger">Reset</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php if (isset($_GET['submit']) && $ulang == false) : ?>
    <div class="container mt-5 mb-5">
      <div class="row justify-content-center">
        <div class="col-12 text-center mb-3">
          <h3>Hasil Perhitungan</h3>
        </div>
        <div class="col-lg-4 col-md-8 col-sm-8  bg-primary text-white p-4">
          <div class="row mb-3 text-center">
            <h5 class="col-12">Pencapaian Siswa</h5>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-6">Nama Siswa</h5>
            <div class="col-sm-6">
              <p><?php echo $nama; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-6">Mata Pelajaran</h5>
            <div class="col-sm-6">
              <p><?php echo $pelajaran; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-6">Nilai UTS</h5>
            <div class="col-sm-6">
              <p><?php echo $_GET['uts'];  ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-6">Nilai UAS</h5>
            <div class="col-sm-6">
              <p><?php echo $_GET['uas']; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-6">Nilai Tugas</h5>
            <div class="col-sm-6">
              <p><?php echo $_GET['tugas']; ?></p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-8 col-sm-8  border border-primary p-4">
          <div class="row mb-3 text-center">
            <h5>Detail Nilai</h5>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-7">Nilai UTS (35%)</h5>
            <div class="col-sm-5">
              <p><?php echo $nilai_uts; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-7">Nilai UAS (50%)</h5>
            <div class="col-sm-5">
              <p><?php echo $nilai_uas; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-7">Nilai Tugas (15%)</h5>
            <div class="col-sm-5">
              <p><?php echo $nilai_tugas; ?></p>
            </div>
          </div>
          <div class="row mb-3">
            <h5 class="col-sm-7">Total Nilai</h5>
            <div class="col-sm-5">
              <p><?php echo $nilai_total; ?></p>
            </div>
          </div>
          <div class="row">
            <h5 class="col-sm-7">Grade Nilai</h5>
            <div class="col-sm-5">
              <p><?php echo $grade; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <footer class="mt-5 mb-5 text-center">
    <p>Copyright 2021 - I Nyoman Triarta</p>
  </footer>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>